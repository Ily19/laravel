<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    </head>
    <body>

       <div class="container">
            <h1>Base de données 1 - Posts<h1>
            @foreach ($posts as $post)
                <h4>{{ $post->title }}</h4>
                <p>{{ $post->content }}</p>
                <hr>
            @endforeach

            <h1>Base de données 2 - Books<h1>
            @foreach ($books as $book)
                <h4>Book writer email</h4>
                <p>{{ $book->email }}</p>
                <hr>
            @endforeach
       </div>
    </body>

    <!-- Styles -->
    <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .container {
                margin-left: 70px;
                margin-right: 70px;
            }

        </style>
</html>
