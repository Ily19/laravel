    <head>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-flash-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.3/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sb-1.0.0/sp-1.2.0/sl-1.3.1/datatables.min.css"/>
    </head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-colvis-1.6.4/b-flash-1.6.4/b-html5-1.6.4/b-print-1.6.4/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.3/r-2.2.6/rg-1.1.2/rr-1.2.7/sc-2.0.3/sb-1.0.0/sp-1.2.0/sl-1.3.1/datatables.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    @section('content')

    <div class="action_buttons">
        <button type="button" class="btn btn-outline-info col-md-4">Exporter la liste</button>
        <button type="button" class="btn btn-outline-success col-md-4">Mise à jour</button>
    </div><hr>

    <table id="table_id" class="display table table-striped">
        <thead class="list_table_head">
            <tr>
                <th scope="col">Identifiant</th>
                <th scope="col">Nom. Prénom</th>
                <th scope="col">Type emploi</th>
                <th scope="col">caisse régional</th>
                <th scope="col">Pôle utilisateur</th>
                <th scope="col">Statut</th>
                <th scope="col">Société</th>
                <th scope="col">Taux de refacruration</th>
                <th scope="col">Taux d'activité</th>
                <th scope="col">Rémuniration (Interne)/Montant facturé(Externe)</th>
                <th scope="col">Montant équipement bureautique</th>
                <th scope="col">Valorisation</th>
            </tr>

        </thead>
        <tbody class="list_table_body">
            <tr>
                <td scope="row">ET00334</td>
                <td>Dupont. Jean</td>
                <td>Interne</td>
                <td>PACA</td>
                <td>CREDIT</td>
                <td>CDI</td>
                <td>-</td>
                <td>75%</td>
                <td>100%</td>
                <td>80000€</td>
                <td>412.3</td>
                <td>60000€</td>
            </tr>

            <tr>
                <td scope="row">ETP0040</td>
                <td>Joubert. Stéphane</td>
                <td>Externe</td>
                <td>PACA</td>
                <td>CREDIT</td>
                <td>-</td>
                <td>CGI</td>
                <td>75%</td>
                <td>-</td>
                <td>45800€</td>
                <td>-</td>
                <td>34350€</td>
            </tr>

            <tr>
                <td scope="row">ET00334</td>
                <td>Dupont. Jean</td>
                <td>Interne</td>
                <td>PACA</td>
                <td>CREDIT</td>
                <td>CDI</td>
                <td>-</td>
                <td>75%</td>
                <td>100%</td>
                <td>80000€</td>
                <td>412.3</td>
                <td>60000€</td>
            </tr>

            <tr>
                <td scope="row">ETP0040</td>
                <td>Joubert. Stéphane</td>
                <td>Externe</td>
                <td>PACA</td>
                <td>CREDIT</td>
                <td>-</td>
                <td>CGI</td>
                <td>75%</td>
                <td>-</td>
                <td>45800€</td>
                <td>-</td>
                <td>34350€</td>
            </tr>
        </tbody>
    </table>


        <style>
            .action_buttons {
                margin-top: 40px;
                margin-bottom: 20px;
            }

            table {
                border-bottom: 1px solid #4e4e4e !important;
            }
            .list_table_head {
                background-color: #6fd4ff;
            }
            .list_table_head th {
                font-size: 14px;
            }
            .list_table_body td {
                font-size: 14px;
            }
        </style>



        <script type="text/javascript">
            $(document).ready( function () {
                //$('#table_id').DataTable();

                $('#table_id').DataTable( {
                    "order": [[ 0, 'asc' ]],
                    responsive: true
                } );
            });

        </script>
    @endsection









