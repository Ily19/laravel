<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     *
     *
     * @return void
     */
    public function mainhome()
    {
        $title = "Mon dashboard";
        $link = [
            '1' => 'Gérer les ressources',
            '2' => 'Gérer les frais'
        ];

        return view('mainhome', [
            'title' => $title,
            'links' => $link
        ]);
    }

    /**
     * here to display the table info
     *
     * @return void
     */
    public function list()
    {
        $list = "here is list";

        return view('list', [
            'list' => $list,
        ]);
    }
}
