<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Function to connect to multiple databases
     *
     * @return void
     */
    public function home()
    {
        $posts = DB::connection('mysql')->table('posts')->get();
        $books = DB::connection('mysql2')->table('Books')->get();


        return view('home', [
            'posts' => $posts,
            'books' => $books
        ]);
    }
}
